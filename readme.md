# Exemple d'utilisation de la CI

[[_TOC_]]

Un exemple pour ...

* bien comprendre les principes de l'intégration continue
* maitriser les outils (gitlab-ci) et être capable de les mettre en place dans un projet gitlab.

## Intégration continue, un premier exemple : construction et mise à disposition d'un pdf

### Objectifs

- produire automatiquement un pdf et le mettre en ligne à partir d'un .tex
- le rendre disponible sur la page du projet Gitlab

Point de départ : 
- ce projet gitlab
- un fichier [article.tex](./article.tex)
- un fichier [gitlab-ci.yml](.gitlab-ci.yml)

contenant les lignes suivantes :

```yaml
make_pdf:
  stage: build
  image: aergus/latex:latest
  script:
     - pdflatex article.tex
  artifacts:
    paths:
      - article.pdf
    expire_in: 1 week
```

Fichier yml :

* langage yaml
* une série de mots-clés et d'attributs définissant les caractéristiques du job. Doc : [gitlab-ci-keywords](https://docs.gitlab.com/ee/ci/yaml/)

Remarque : les differentes versions du .yml sont dans le répertoire [modeles](./modeles)

:information_source: *si vous ajoutez ce fichier via l'interface gitlab (ou le WebIDE) vous pourrez profiter des templates de fichier .gitlab-ci.yml*  

La présence de ce fichier dans le repository va entrainer :

* l'activation de l'intégration continue
* la création et l'exécution d'un **job** nommé make_pdf

    * mot-clé **script** : les commandes à exécuter
    * mot-clé **image** : permet de définir l'environnement où seront exécutées les commandes
    * mot-clé **artifacts** : ce qui doit être conservé à la fin

### Vocabulaire

* **job** : une suite d’actions exécutées sur un runner.
  Chaque job est indépendant. Rien n’est conservé à la fin du job (sauf demande explicite via les artifacts).

* **runner** : une machine hôte qui va exécuter Docker et faire tourner notre job.

* **Pipeline** : une série de jobs exécutés sur un ou plusieurs runners.

  Chaque pipeline correspond à un commit et vous retrouvez dans le pipeline les jobs définis dans votre script (make_pdf dans votre cas).

* **stage** : un “étage” du pipeline, pouvant contenir plusieurs job exécutés en parallèle.
  
  * Pour passer au stage suivant, tous les jobs doivent avoir réussi (sauf en cas d'utilisation du mot-clé **needs**).
  * Chaque job doit faire partie d'un stage.
  * Pour plus de détails, voir [gitlab-ci stages](https://docs.gitlab.com/ee/ci/yaml/#stage)

* **artifacts** : des répertoires ou des fichiers à conserver et à transmettre d’un job à l’autre.

:warning: ces artifacts sont conservés et stockés sur la plateforme et, selon les projets, peuvent rapidement occuper beaucoup d'espace.

**expire_in** : fixe une durée de vie et assure la destruction des artifacts au bout d'une semaine. Cela permet d'éviter d'encombrer le serveur inutilement.
Pour plus de détails à ce sujet, voir [Defining job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).


### Résultats

Pour vérifier l'impact de l'ajout du fichier .gitlab-ci.yml, visitez le menu CI/CD->pipelines du projet.

Dans le menu CI/CD->pipelines, si vous cliquez sur un pipeline puis sur un job, vous obtiendrez un accès direct à la console. Par exemple :

![gitlab-ci-jobs.jpeg](images/gitlab-ci-jobs.jpeg)

Lorsque vous êtes sur la page du job, vous pouvez également naviguer dans les artifacts (menu à droite) et avoir accès aux fichiers générés et conservés, article.pdf dans notre exemple.

## Quelques commentaires

* Pour chaque job, "clonage" automatique du projet sur l'hôte

* Durant le job, le répertoire du projet est identifié par la variable CI_PROJECT_DIR.

A noter que de nombreuses variables d'environnement sont définies par défaut par gitlab-ci et utilisables dans vos scripts, voir [GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/).

:information_source: exécutez la commande **env** dans votre script pour afficher toutes les variables d'environnement :
```yml
  script:
     - env
     - pdflatex article.tex
```

* Attention à la syntaxe, le yaml peut-être pénible ...

Nous vous conseillons d'utiliser le menu **CI/CD Editor** pour éditer votre fichier .gitlab-ci.yml. Il dispose d'un onglet 'lint' qui vous permettra
de vérifier et de valider la syntaxe de votre script. 

![](./images/ci-lint.jpg)

Faites le test !



## Mise en ligne et création d'une page web : **gitlab-pages**

### Principe

Un outil permettant de publier un site web statique associé à votre projet : [gitlab-pages](https://about.gitlab.com/features/pages/).

1. Ecriture de fichiers/pages dans un langage type markdown ou rst et sauvegardez (git) ces fichiers dans votre projet gitlab.
2. Un outil de votre choix (sphinx, mkdocs ...) est appelé dans un job d'**intégration continue** pour générer des pages web **statiques** (fichiers html et autres). A noter que cette étape est facultative : vous pourriez sauvegarder directement des fichiers html dans votre projet git.
3. Un job d'intégration continue spécifique qui DOIT s'appeler *pages* est chargé de publier vos pages sur un serveur qui sera nommé
 <NOM_DE_VOTRE_GROUPE>.gricad-pages.univ-grenoble-alpes.fr/<NOM_SOUS_GROUPE>/<NOM_PROJET>
  
  Voici quelques exemples de pages publiées via gitlab-pages :

  * [Les pages web de nos formations](https://pole-calcul-formation.gricad-pages.univ-grenoble-alpes.fr/ced)
  * [Les pages de doc d'un code de calcul](https://nonsmooth.gricad-pages.univ-grenoble-alpes.fr/siconos/)


Nous allons compléter notre projet (celui avec article.tex) pour créer une page web contenant entre un lien vers le pdf généré par le job make_pdf.

Les sources (markdown) pour générer la page sont dans le répertoire doc. Nous utilisons mkdocs, un outil qui permet de créer du contenu web
à partir de markdown.

* ajouter au fichier .gitlab-ci.yml les lignes suivantes :

```yaml
pages:
  stage: test
  image: python:3.8.5-slim-buster
  script:
  - python -m pip install --upgrade pip mkdocs-bootswatch
  - mkdocs build
  - mv site public
  artifacts:
    paths:
    - public
```

Comme pour le job make_pdf, on retrouve :
* un stage (test)
* une image docker sur laquelle le job sera exécuté, ici python:3.8.5-slim-buster
* une série de commandes après le mot-clé **script**
* des artifacts : ici nous souhaitons conserver le repertoire 'public' qui contient tous les fichiers html générés par pandoc.

#### Quelques commentaires

* Pour créer une page web, le job doit s'appeler pages
* Un job 'deploy' sera automatiquement créé et associé à pages. Son rôle est de 'déployer' les pages web à un endroit accessible

:point_right: vérifiez la présence de ce job dans le pipeline

* l'adresse du site web créé sera toujours de la forme NOM_GROUPE.gricad-pages.univ-grenoble-alpes.fr/NOM_PROJET. Vous pouvez retrouver cette info dans les Settings du projet, menu pages
* Vous pouvez utiliser n'importe quel outil pour créer votre site web (hugo, mkdocs, sphinx ...). Le point important est que tous les fichiers générés doivent se trouver dans le répertoire 'public' passé dans les artifacts.


#### Résultat

Si tout fonctionne correctement, vous aurez accès à votre site web via le lien suivant :
https://gtdonnees-gitlab2023.gricad-pages.univ-grenoble-alpes.fr/exemple-ci-1/

La page web créée peut-être publique ou privée (i.e. accessible uniquement aux membres du projet après authentification). 
Le contrôle de cet accès se fait via le menu "Settings, General, Features etc, Pages. 

:point_right:  Faites le test : changez les droits de vos pages, allez visiter le site de votre voisin.

## Compléments

Sur de gros projets, l'intégration continue peut rapidement devenir consommatrice de ressoures :frowning:

De plus il n'est pas toujours judicieux ou utile de lancer systématiquement ou entièrement la CI après chaque modification du code source.
On peut par exemple souhaiter modifier la doc sans lancer la recompilation du code, éditer des fichiers qui n'auront pas d'impact sur les pages web etc

Il existe différentes manières de contrôler la CI, entre autres :

* via le contenu des messages de commit.
  Par exemple, si le message commence par [skip CI], les jobs ne sont pas lancés. 
 
* en précisant explicitement des règles de fonctionnement dans le fichier yml.

Par exemple, en ajoutant au début du fichier .gitlab-ci.yml les lignes suivantes :

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /\[publish\].*/i
      when: always
    - when: never
```

Dans ce cas, si le message de commit contient [publish],  la règle est de lancer la CI. Dans le cas contraire, rien ne se passe.

:warning: fixer des règles est une bonne habitude à prendre pour éviter de lancer tout le process au moindre changement dans le code.
Rappelez vous de [Je code: les bonnes pratiques en écoconception](https://scalde.gricad-pages.univ-grenoble-alpes.fr/comm-et-formations/2021_02_11-atelier-outils-collaboratifs/pdf/Plaquette_Ecoinfo_EcoConception_Logicielle_PDF_v1.0.pdf) !


